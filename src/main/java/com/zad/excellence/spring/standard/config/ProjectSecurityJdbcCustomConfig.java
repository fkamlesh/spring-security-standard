package com.zad.excellence.spring.standard.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;

/*
 * Consider CORS while Integrating with UI:
 * Consider Disabling CSRF using (.csrf().disable()) OR
 * Resolving CSRF By generating CSRF Token in the backend and maintain the Same in UI: 
 */

@Configuration
@EnableWebSecurity
public class ProjectSecurityJdbcCustomConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private CustomCorsConfiguration corsConfiguration;

    @Override
    public void configure(WebSecurity webSecurity) throws Exception {
        webSecurity.ignoring().antMatchers("/api/contact/**").antMatchers("/api/notices/**");
    }

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        httpSecurity.cors().configurationSource(corsConfiguration)
        .and()
        //.csrf().disable()
        .csrf().ignoringAntMatchers("/contact").csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse())
        .and()
        .authorizeRequests().anyRequest()
                .authenticated().and().formLogin().and().httpBasic();
    }

    @Bean
    public PasswordEncoder customPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

}
