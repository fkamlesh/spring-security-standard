package com.zad.excellence.spring.standard.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.zad.excellence.spring.standard.entity.Customers;
import com.zad.excellence.spring.standard.model.SecurityCustomer;
import com.zad.excellence.spring.standard.repository.CustomerRepository;

/*
 * The Class : MyApplicationSecurityUserDetails : Is Custom Implementation 
 * For UserDetailsService to provide UserDetails for Authentication.
 * loadUserByUsername() Method has been overriden to write custom logic to fetch Users:
*/

@Service
public class MyApplicationSecurityUserDetails implements UserDetailsService {
    
    @Autowired
    private CustomerRepository customerRepository;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        List<Customers> customers = customerRepository.findByEmail(email);        
        if (customers.size() == 0) {
            throw new UsernameNotFoundException("User Details not found for the User : " + email);
        }
        return new SecurityCustomer(customers.get(0));
    }

}
