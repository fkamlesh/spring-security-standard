package com.zad.excellence.spring.standard;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringSecurityStandardSection1Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringSecurityStandardSection1Application.class, args);
	}

}
