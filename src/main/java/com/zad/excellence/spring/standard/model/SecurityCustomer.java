package com.zad.excellence.spring.standard.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.zad.excellence.spring.standard.entity.Customers;

public class SecurityCustomer implements UserDetails {

    private final Customers customers;

    public SecurityCustomer(Customers customers) {
        super();
        this.customers = customers;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority(customers.getRole()));
        return authorities;
    }

    @Override
    public String getPassword() {
        return customers.getPwd();
    }

    @Override
    public String getUsername() {
        return customers.getEmail();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

}
