# A Microservice application Build to Use: 

# CSRF is not Disable, we can't access this Backend from POSTMAN.


=> Spring Security Basic Configuration with Defining and Managing Users:
   
1. ProjectSecurityJdbcCustomConfig : JDBC Related Configuration, Users are saved in Table under 
   Custom Defined Table. In Our case, Customer Table. Here  Configuration, MyApplicationSecurityUserDetails
   Class and Custom table (Customer) Mapping is Important.

2. CustomCorsConfiguration : Custom Cors related Configuration is defined and used main 
   Configuration class.
   
3. CSRF is not disable, CSRF token generation and validation is handled in main configuration.
   We can also disable CSRF and test the UI application Integration.  

Please Note: Updated authorities Table has been created with two rows:
   
Reference: 
https://pkglobal.udemy.com/course/spring-security-zero-to-master/learn/lecture/22460850#overview

